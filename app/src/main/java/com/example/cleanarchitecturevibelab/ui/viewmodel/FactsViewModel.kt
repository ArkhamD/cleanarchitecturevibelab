package com.example.cleanarchitecturevibelab.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cleanarchitecturevibelab.data.CatFact
import com.example.cleanarchitecturevibelab.network.RetrofitBuilder
import com.example.cleanarchitecturevibelab.ui.viewmodel.FactsUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class FactsViewModel: ViewModel() {
    private val _uiState = MutableStateFlow(FactsUiState())
    val uiState: StateFlow<FactsUiState> = _uiState.asStateFlow()

    init {
        _uiState.value = FactsUiState(listOf(
            CatFact("Fact", 1),
            CatFact("Fact", 1),
            CatFact("Fact", 1),
            CatFact("Fact", 1),
            CatFact("Fact", 1)
        ))
    }

    fun getFacts(num: Int) {
        viewModelScope.launch {
            val facts = RetrofitBuilder.getInstance().getCatFacts(num).data
            _uiState.update {
                it.copy(
                    currentFacts = facts
                )
            }
        }
    }
}