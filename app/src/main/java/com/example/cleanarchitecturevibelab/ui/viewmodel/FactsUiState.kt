package com.example.cleanarchitecturevibelab.ui.viewmodel

import com.example.cleanarchitecturevibelab.data.CatFact

data class FactsUiState(
    val currentFacts: List<CatFact> = listOf(CatFact("", 1))
)