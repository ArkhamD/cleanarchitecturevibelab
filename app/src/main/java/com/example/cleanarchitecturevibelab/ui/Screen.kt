package com.example.cleanarchitecturevibelab.ui

import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.cleanarchitecturevibelab.R
import com.example.cleanarchitecturevibelab.data.CatFact
import com.example.cleanarchitecturevibelab.ui.theme.CleanArchitectureVIBELABTheme
import com.example.cleanarchitecturevibelab.ui.viewmodel.FactsViewModel
import com.example.cleanarchitecturevibelab.ui.viewmodel.ScreenState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Screen(
    modifier: Modifier = Modifier
) {
    val screenState = rememberSaveable { mutableStateOf(ScreenState.FactScreen) }

    Scaffold(
        topBar = { TopBar(
            text = if (screenState.value == ScreenState.WebScreen) "Api info" else "Facts"
        ) },
        bottomBar = { BottomBar(switchScreens = { screenState.value =
        if (screenState.value == ScreenState.FactScreen) ScreenState.WebScreen else ScreenState.FactScreen}) },
        modifier = modifier
    ) {
        when(screenState.value) {
            ScreenState.FactScreen -> FactScreen(paddingValues = it)
            else -> WebScreen()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FactScreen(
    paddingValues: PaddingValues,
    modifier: Modifier = Modifier,
    factsViewModel: FactsViewModel = viewModel()
) {
    val gameUiState by factsViewModel.uiState.collectAsState()

    Column(
        modifier = modifier
            .padding(paddingValues)
            .fillMaxSize()
            .padding(top = 20.dp, bottom = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ListFacts(facts = gameUiState.currentFacts,
            modifier = Modifier
                .weight(0.7f)
        )

        var countValue by remember { mutableStateOf("") }
        Column(
            modifier = Modifier.weight(0.25f),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            OutlinedTextField(
                value = countValue,
                onValueChange = {countValue = it},
                placeholder = {
                    Text(text = "Count")
                },
                modifier = Modifier.padding(16.dp)
            )
            Button(
                onClick = {
                    val intCountValue = countValue.toIntOrNull()
                    if (intCountValue != null) factsViewModel.getFacts(intCountValue)
                },
                shape = RectangleShape
            ) {
                Text(text = "RELOAD")
            }
        }
    }
}

@Composable
fun WebScreen(
    modifier: Modifier = Modifier
) {
    val mUrl = "https://www.google.com/"
    var webView: WebView? = null
    val webViewState = rememberSaveable { mutableStateOf(mUrl) }
    val backEnabled = rememberSaveable { mutableStateOf(false) }

    AndroidView(
        factory = { context ->
            WebView(context).apply {
                webViewClient = object : WebViewClient() {

                    override fun onPageFinished(view: WebView?, url: String?) {
                        webViewState.value = url ?: ""
                    }
                    override fun onPageStarted(view: WebView, url: String?, favicon: Bitmap?) {
                        backEnabled.value = view.canGoBack()
                    }
                }
                loadUrl(webViewState.value)
            }
        },
        update = {
            webView = it
        }
    )

    BackHandler(enabled = backEnabled.value) {
        webView?.goBack()
    }
}

@Composable
fun Fact(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        fontSize = 18.sp,
        modifier = modifier
            .padding(
                start = 16.dp,
                end = 16.dp
            )
            .fillMaxWidth()
    )
}

@Composable
fun ListFacts(
    facts: List<CatFact>,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier,
    ) {
        items(facts) { fact ->
            Fact(text = fact.fact, modifier = Modifier.padding(bottom = 16.dp))
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(text: String, modifier: Modifier = Modifier) {
    TopAppBar(
        title = {
                Text(
                    text = text
                )
        },
        modifier = modifier,
        colors = TopAppBarDefaults.smallTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primary,
            titleContentColor = Color.White
        )
    )
}

@Composable
fun BottomBar(switchScreens: () -> Unit, modifier: Modifier = Modifier) {
    BottomAppBar(
        modifier = modifier,
        containerColor = MaterialTheme.colorScheme.background
    ) {
        Row(
            modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            IconButtonBottomBar(
                iconId = R.drawable.buffoon,
                text = "Facts",
                switchScreens = { switchScreens() }
            )
            IconButtonBottomBar(
                iconId = R.drawable.web,
                text = "Web",
                switchScreens = { switchScreens() }
            )
        }
    }
}

@Composable
fun IconButtonBottomBar(
    iconId: Int,
    text: String,
    switchScreens: () -> Unit,
    modifier: Modifier = Modifier) {
    IconButton(
        onClick = { switchScreens() },
        modifier = modifier
            .fillMaxHeight()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                painter = painterResource(id = iconId),
                contentDescription = null
            )
            Text(
                text = text,
                maxLines = 1
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    CleanArchitectureVIBELABTheme {
        Screen()
    }
}