package com.example.cleanarchitecturevibelab.ui.viewmodel

enum class ScreenState {
    FactScreen, WebScreen
}